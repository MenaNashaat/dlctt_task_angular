export interface UserDetails {

    name?: string;
    id?: number;
    username?: string;
    email?: string;
    phone?: string;
    website?: string;
    street?: string;
    suite?: string;
    city?: string;
    zipcode?: string;
    catchPhrase?: string;
    bs?: string;
    
   
}