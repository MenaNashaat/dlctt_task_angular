import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { Post } from '../../Interfaces/post';
@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http : HttpClient) { }
  readonly rootUrl = "https://jsonplaceholder.typicode.com/posts";
  getPosts():Observable<Post[]>{

  
    return this.http.get<Post[]>(this.rootUrl);
      
    }
    getPostbyId(id : number) {

      return this.http.get(this.rootUrl+'/'+id)
    }
}
