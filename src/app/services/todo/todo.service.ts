import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { todo } from '../../Interfaces/todo';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  base_url: string = 'https://jsonplaceholder.typicode.com'; 
  constructor(private http : HttpClient) { }


  getTodos(): Observable<todo[]>{
    console.log('getting all todos from the server');
    return this.http.get<todo[]>(`${this.base_url}/todos`);
  }
  addTodo(newTodo: todo): Observable<todo>{
    return this.http.post<todo>(`${this.base_url}/todos`, newTodo);
  }
 /** DELETE: delete the hero from the server */
 deleteTodo (hero: todo | number): Observable<todo> {
  const id = typeof hero === 'number' ? hero : hero.id;
  const url = `${this.base_url}/todos/${id}`;
  return this.http.delete<todo>(url)
}
/** GET hero by id. Will 404 if id not found */
getHero(id: number): Observable<todo> {
  const url = `${this.base_url}/todos/${id}`;
  return this.http.get<todo>(url);
}
/** PUT: update the hero on the server */
updateTodo (hero: todo): Observable<todo> {
  return this.http.put<todo>(`${this.base_url}/todos/${hero.id}`, hero)
}

}
