import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {
  isCollapse: boolean = false;
  title = 'dlctt';
  constructor(private titleService: Title, private Router: Router ) { }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }


  toggleNavBar() {
    this.isCollapse = !this.isCollapse;
  }
  Loqout()
  {
    this.Router.navigate(['/login']);
  }

  ngOnInit() {
  }

}
