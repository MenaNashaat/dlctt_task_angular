import { Component, OnInit } from '@angular/core';
import { TodoService } from '../../services/todo/todo.service'
import {todo} from '../../Interfaces/todo' 
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {
  todoList:  todo[];
  constructor(private todoService : TodoService) { }

  ngOnInit() {
    this.getHeroes();
  }
  getHeroes(): void {
    this.todoService.getTodos().subscribe(data=> this.todoList = data)
  }
  add(name: string): void {
    name  = name.trim();
    if (!name) { return; }
    
    this.todoService.addTodo( {name} as todo )
      .subscribe(data => {
        this.todoList.push(data);
        console.log("data"+data.title)
      });
  }
  //delete a todo
  delete(item: todo): void {
    this.todoList = this.todoList.filter(h => h !== item);
    this.todoService.deleteTodo(item).subscribe();
  }


}
