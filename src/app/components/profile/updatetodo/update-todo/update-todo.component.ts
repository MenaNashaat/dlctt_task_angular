import { Component, OnInit , Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { TodoService } from '../../../../services/todo/todo.service';
import {todo} from '../../../../Interfaces/todo' ;
@Component({
  selector: 'app-update-todo',
  templateUrl: './update-todo.component.html',
  styleUrls: ['./update-todo.component.sass']
})
export class UpdateTodoComponent implements OnInit {
  @Input() hero: todo;
  
  constructor(private route: ActivatedRoute,
    private todoService: TodoService,
    private location: Location) { }
    

  ngOnInit() {
    //const id = +this.route.snapshot.paramMap.get('id');
    this.getHero();
    
  }
  getHero(): void {
    
    const id = + this.route.snapshot.paramMap.get('id');;
    this.todoService.getHero(id)
      .subscribe(hero => this.hero = hero);
  }
  goBack(): void {
    this.location.back();
  }

 save(): void {
    this.todoService.updateTodo(this.hero)
      .subscribe(() => this.goBack());
   
  }

}
