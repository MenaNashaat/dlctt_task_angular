import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { PostService } from '../../../services/post/post.service';
import { PostDetails } from '../../../Interfaces/postDetails';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.sass']
})
export class PostDetailsComponent implements OnInit {

  constructor( private postService: PostService, private activatedRoute: ActivatedRoute, private router: Router) { }
  postId;
  post: PostDetails;

  ngOnInit() {

    this.activatedRoute.paramMap.subscribe(params => {
      let postId = this.postId = + params.get('post-id')
      if (this.postId) {
        this.postService.getPostbyId(postId).subscribe(response => {
          this.post = response;
        });

      }
      else {
        this.router.navigate(['posts', 1]);
      }

    });
  }

}
