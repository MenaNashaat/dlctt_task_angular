import { Component, OnInit } from '@angular/core';
import { PostService } from '../../../services/post/post.service';
import { Post } from '../../../Interfaces/post';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.sass']
})
export class PostsListComponent implements OnInit {
  post: Post[] = [];
  postFilter: any = { body: '' };

  constructor(private postService: PostService ) { }

  ngOnInit() {
    this.postService.getPosts().subscribe(data=> this.post= data)
  }

}
