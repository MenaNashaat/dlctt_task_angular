import { Component, OnInit, DoCheck, AfterViewChecked } from '@angular/core';
import { UsersService } from '../../../services/user/users.service';
import { User } from '../../../Interfaces/user';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass']
})
export class UsersComponent implements OnInit {


  users: User[] = [];
  userId: number;

  userFilter: any = { name: '' };


  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.usersService.getUsers().subscribe((users: User[]) => {
      this.users = users;

    });

  
    
  }

  

 


}









