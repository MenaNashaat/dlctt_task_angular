import { UserDetailsComponent } from './components/user/user-details/user-details.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './components/user/users-list/users.component';
import { PostsListComponent } from './components/post/posts-list/posts-list.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AuthGuard } from './guards/auth.guard';
import { UpdateTodoComponent } from './components/profile/updatetodo/update-todo/update-todo.component';

const routes: Routes = [

  {path :" ", component : HomeComponent },
  {path :"home", redirectTo: ' ' , pathMatch : 'prefix'},
  {path: "login" , component : LoginComponent},
  {path: "profile" , component : ProfileComponent , canActivate: [AuthGuard] },
  { path : "users" , component :UsersComponent}, 
  { path : "users/:user-id" , component : UsersComponent},
  { path : "users/**" , redirectTo: 'users' , pathMatch : 'prefix'},
  { path : "todo/:id" , component : UpdateTodoComponent},
  { path : "posts" , component :PostsListComponent},
  { path : "posts/:post-id" , component : PostsListComponent},
  { path : "posts/**" , redirectTo: 'posts' , pathMatch : 'prefix'},
  {path : '**', component : HomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
