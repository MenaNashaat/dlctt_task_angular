import { BrowserModule , Title} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { UsersComponent } from './components/user/users-list/users.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import {HttpClientModule} from '@angular/common/http';
import { UserDetailsComponent } from './components/user/user-details/user-details.component';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { FormsModule } from '@angular/forms';
import { PostsListComponent } from './components/post/posts-list/posts-list.component';
import { PostDetailsComponent } from './components/post/post-details/post-details.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AuthGuard } from './guards/auth.guard';
import {TodoService} from './services/todo/todo.service';
import { UpdateTodoComponent } from './components/profile/updatetodo/update-todo/update-todo.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
   
    UsersComponent,
    NavbarComponent,
    UserDetailsComponent,
    PostsListComponent,
    PostDetailsComponent,
    LoginComponent,
    ProfileComponent,
    UpdateTodoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    
    FilterPipeModule
    
   
  ],
  providers: [AuthGuard, TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
